with (import <nixpkgs> {});

stdenv.mkDerivation {
    name = "riscv32-inria-elf";
    version = "0.1";

    src = fetchurl {
        url = "https://gitlab.inria.fr/rlasherm/riscv_gcc_nix/-/raw/master/riscv-medlow.tar.xz?inline=false";
        hash =  "sha256-3IXiUITaDpykFPvo06F9a/qW/yLG+aau4cKJVRYoMD8=";
    };

    nativeBuildInputs = [autoPatchelfHook ];

    phases = [ "unpackPhase" "installPhase" ];

    installPhase = ''
        mkdir -p $out
        cp -r * $out
    '';
  
}

