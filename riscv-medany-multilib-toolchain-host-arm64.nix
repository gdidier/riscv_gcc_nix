with (import <nixpkgs> {});

stdenv.mkDerivation {
    name = "riscv32-inria-elf";
    version = "0.1";

    src = fetchurl {
        url = "https://gitlab.inria.fr/rlasherm/riscv_gcc_nix/-/raw/master/riscv-medany-host-arm64.tar.xz?inline=false";
        hash =  "";
    };

    nativeBuildInputs = [autoPatchelfHook ];

    phases = [ "unpackPhase" "installPhase" ];

    installPhase = ''
        mkdir -p $out
        cp -r * $out
    '';
  
}

